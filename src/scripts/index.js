import '../styles/index.scss';
import * as d3 from 'd3';
import inputData from './data.json';
import { addGrid, addLabels } from './utils/chart';

const svgElement = d3.select('#chart');

// Config for basic layout things

const margin = ({
  top: 20,
  bottom: 70,
  left: 70,
  right: 20
});

const width = 1200 - margin.left - margin.right;
const height = 700 - margin.top - margin.bottom;

// Set the width and height of our SVG element based on our config values
svgElement.attr('width', width + margin.left + margin.right)
  .attr('height', height + margin.top + margin.bottom);

// Create a group element within the SVG - this will be what we add everything to
const rootGroup = svgElement.append('g');

// Offset the group by our margin values - this saves us having to remember to add the margin to every element individually
rootGroup.attr('transform', `translate(${margin.left}, ${margin.top})`);



/////////
// CREATE SOME MATH FUNCTIONS
/////////



/////////
// DRAW AXES
/////////

// TODO: uncomment this to draw the year axis

/*
const yearAxisContainer = rootGroup.append('g')
  .attr('class', 'Axis')
  .attr('transform', `translate(0, ${height + 10})`);

const yearAxis = d3.axisBottom(yearScale)
  .ticks(d3.timeYear.every(5))
  .tickSizeOuter(0)
  .tickSizeInner(0);

yearAxis(yearAxisContainer);
*/

// TODO: Add the score axis here



/////////
// DRAW GRID LINES AND LABELS
/////////

// The code for these isn't super exciting, so I've extracted them to util functions.
// Feel free to take a look at them if you're curious though!

/*
addGrid({
  rootGroup,
  xScale: yearScale,
  yScale: scoreScale,
  width,
  height,
});

addLabels({
  rootGroup,
  margins: margin,
  height,
  width,
  xLabel: 'Release year',
  yLabel: 'Metacritic score',
});
*/



/////////
// DRAW THE CHART CONTENT
/////////



/////////
// LISTENER SET-UP
/////////

document.getElementById('boxOfficeSelect').addEventListener('change', () => {
  // TODO: call our drawing function here
});

document.getElementById('scoreSelect').addEventListener('change', () => {
  // TODO: call our drawing function here
});