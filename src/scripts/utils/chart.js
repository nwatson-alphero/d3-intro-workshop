// CHART DRAWING UTIL FUNCTIONS

export const addGrid = ({ rootGroup, xScale, yScale, width, height }) => {
  // Creates a `line` element for every "tick" in the provided scale
  rootGroup.selectAll('.Grid--vertical')
    .data(yScale.ticks())
    .enter()
    .append('line')
    .attr('class', 'Grid Grid--vertical')
    .attr('x1', 0)
    .attr('x2', width)
    .attr('y1', d => yScale(d))
    .attr('y2', d => yScale(d));

rootGroup.selectAll('.Grid--horizontal')
  .data(xScale.ticks())
  .enter()
  .append('line')
  .attr('class', 'Grid Grid--horizontal')
  .attr('y1', 0)
  .attr('y2', height)
  .attr('x1', d => xScale(d))
  .attr('x2', d => xScale(d));
};

export const addLabels = ({ rootGroup, margins, height, width, xLabel, yLabel }) => {
  rootGroup.append('text')
    .attr('transform', 'rotate(-90)')
    .attr('y', 0 - margins.left)
    .attr('x',0 - (height / 2))
    .attr('dy', '1em')
    .style('text-anchor', 'middle')
    .text(yLabel);

rootGroup.append('text')
  .attr('x', width / 2)
  .attr('y',(height + margins.bottom - 15))
  .style('text-anchor', 'middle')
  .text(xLabel);
};
