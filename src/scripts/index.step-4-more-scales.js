import '../styles/index.scss';
import * as d3 from 'd3';
import inputData from './data.json';
import { addGrid, addLabels } from './utils/chart';

const svgElement = d3.select('#chart');

// Config for basic layout things

const margin = ({
  top: 20,
  bottom: 70,
  left: 70,
  right: 20
});

const width = 1200 - margin.left - margin.right;
const height = 700 - margin.top - margin.bottom;

// Set the width and height of our SVG element based on our config values
svgElement.attr('width', width + margin.left + margin.right)
  .attr('height', height + margin.top + margin.bottom);

// Create a group element within the SVG - this will be what we add everything to
const rootGroup = svgElement.append('g');

// Offset the group by our margin values - this saves us having to remember to add the margin to every element individually
rootGroup.attr('transform', `translate(${margin.left}, ${margin.top})`);



/////////
// CREATE SOME MATH FUNCTIONS
/////////

const scoreMin = 0; // minimum possible movie score
const scoreMax = 100; // maximum possible movie score

// Creates a function that will remap a score value (between 0 and 100) to a y-position value (between 0 and `height`)
const scoreScale = d3.scaleLinear([scoreMin, scoreMax], [height, 0]);

// Same again for the year scale

const yearMin = d3.min(inputData, item => new Date(item.release_date));
yearMin.setFullYear(yearMin.getFullYear() - 3); // offset our min year a litte so the chart has some padding

const yearMax = d3.max(inputData, item => new Date(item.release_date));
yearMax.setFullYear(yearMax.getFullYear() + 2);

const yearScale = d3.scaleTime([yearMin, yearMax], [0, width]);

// And again for the circle size

const boxOfficeMax = d3.max(inputData, item => item.worldwide_box_office);
const boxOfficeScale = d3.scaleLinear([0, boxOfficeMax], [0, 1000]);

// and finally a scale to map from movie series name to a colour

const seriesNames = inputData.map(d => d.series); // get an array containing all the "series" values

// We use an `ordinal` scale here. This type of scale maps from a set of discrete values, rather than a continuous range of numbers
const colourScale = d3.scaleOrdinal(seriesNames, [
  '#ff7e4b',
  '#00C3B0',
  '#fbc738',
  '#6ed926',
]);



/////////
// DRAW AXES
/////////

// TODO: uncomment this to draw the year axis

/*
const yearAxisContainer = rootGroup.append('g')
  .attr('class', 'Axis')
  .attr('transform', `translate(0, ${height + 10})`);

const yearAxis = d3.axisBottom(yearScale)
  .ticks(d3.timeYear.every(5))
  .tickSizeOuter(0)
  .tickSizeInner(0);

yearAxis(yearAxisContainer);
*/

// TODO: Add the score axis here



/////////
// DRAW GRID LINES AND LABELS
/////////

// The code for these isn't super exciting, so I've extracted them to util functions.
// Feel free to take a look at them if you're curious though!

/*
addGrid({
  rootGroup,
  xScale: yearScale,
  yScale: scoreScale,
  width,
  height,
});

addLabels({
  rootGroup,
  margins: margin,
  height,
  width,
  xLabel: 'Release year',
  yLabel: 'Metacritic score',
});
*/



/////////
// DRAW THE CHART CONTENT
/////////

const drawChart = () => {

  // select all circle elements within our chart. This will be empty the first time we run it, because we haven't added anything yet
  const earningsCircles = rootGroup.selectAll('.EarningsCircle');

  earningsCircles.data(inputData)
    .join(
      (entering) => entering.append('circle')
        .attr('class', 'EarningsCircle')
        .attr('fill', d => colourScale(d.series))
        .attr('r', d => Math.sqrt(boxOfficeScale(d.domestic_box_office_adjusted)))
        .attr('cx', d => yearScale(new Date(d.release_date)))
        .attr('cy', d => scoreScale(d.metacritic)),

      (updating) => updating,

      (exiting) => exiting.remove()
    );
};

// Perform the first drawing of the chart
drawChart();

/////////
// LISTENER SET-UP
/////////

document.getElementById('boxOfficeSelect').addEventListener('change', () => {
  // TODO: call our drawing function here
});

document.getElementById('scoreSelect').addEventListener('change', () => {
  // TODO: call our drawing function here
});