# d3 Workshop

Walkthrough for creating a simple visualisation using d3. This probably won't make much sense if you're not in the workshop that goes along with this.

Each major step of the walkthrough corresponds to one of the `index.step-x` files.

Base project built on top of https://github.com/wbkd/webpack-starter

### Installation

```
nvm use
yarn install
```

### Start Dev Server

```
yarn start
```

### Build Prod Version

```
yarn run build
```
